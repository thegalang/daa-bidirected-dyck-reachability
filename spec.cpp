/*

Source code ini diambil dan dimodifikasi dari source code untuk mengenerate test-case soal "Berburu Susu" pada babak final JCPC COMPFEST 12
*/
#include <ext/rope>
#include <bits/stdc++.h>
#include <tcframe/runner.hpp>
#include <tcrand/graph.hpp>
#include <tcrand/tree.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/detail/standard_policies.hpp>

using namespace std;
using namespace tcframe;
using namespace __gnu_cxx;
using namespace __gnu_pbds;
using namespace tcrand;

#define pb push_back
#define mp make_pair

typedef long long ll;
typedef pair<ll, ll> ii;
typedef pair<ll,ll> pii;
typedef tree<ll, null_type, less<ll>, rb_tree_tag, tree_order_statistics_node_update> ordered_set;

struct query {
	ll x, y, z;
};

struct ufds {
	int parent;
	vector<int> components;
	int maxEdge;
};

#define MAXN 100000

class ProblemSpec : public BaseProblemSpec {
protected:
	ll N, M, K, Q;
	vector<int> U, V, X, Y, L;
	vector<ll> jaw;

	vector<string> ans;
	void InputFormat() {
		LINE(N, M, K);
		LINES(U, V, L) % SIZE(M);
		LINE(Q);
		LINES(X, Y) % SIZE(Q);
	}

	void OutputFormat() {
		LINES(ans);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000000);
		CONS(1 <= Q && Q <= 100000);
		CONS(0 <= M && M <= 1000000);
		CONS(1 <= K && K <= 8);
		CONS(eachElementBetween(U, 0, N-1));
		CONS(eachElementBetween(V, 0, N-1));
		CONS(eachElementBetween(L, 1, K));
		CONS(eachElementBetween(X, 0, N-1));
		CONS(eachElementBetween(Y, 0, N-1));
		CONS(unique(U, V));
		CONS(noLoop(U, V));
	}



private:
	bool eachElementBetween(const vector<int> &A, ll lo, ll hi) {
		for (ll x : A)
			if (x < lo || x > hi) {
				//cerr<<"ERROR BANG "<<x<<" "<<lo<<" "<<hi<<"\n";
				return false;
			}
		return true;
	}

	bool unique(const vector<int> &A, const vector<int> &B) {
		set<ii> tuples;
		for (ll i = 0; i < (ll) A.size(); i++) {
			ii item = mp(A[i], B[i]);
			if (tuples.count(item))
				return false;
			tuples.insert(item);
		}
		return true;
	}

	bool noLoop(const vector<int> &A, const vector<int> &B) {
		for(ll temp=0;temp<A.size();temp++)
			if(A[temp] == B[temp]) return false;
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
	void SampleTestCase1() {
	   	Input({
	   		"6 5 2",
			"1 2 1",
			"1 4 1",
			"3 2 1",
			"3 4 2",
			"5 4 2",
			"3",
			"1 2",
			"3 5",
			"1 4"
	   	});
	   	Output({
	   		"equivalence classes:",
			"{ 0 }",
			"{ 2 }",
			"{ 4 }",
			"{ 1 3 5 }",
			"1 and 2 is not Dyck-reachable!",
			"3 and 5 is Dyck-reachable!",
			"1 and 4 is not Dyck-reachable!",
	   	});
	}

	void BeforeTestCase() {
		U.clear();
		V.clear();
		L.clear();
		X.clear();
		Y.clear();
		

	}

	void AfterTestCase() {

		X.push_back(1);
		Y.push_back(2);
		Q = 1;

		L = randomArray(M, 1, 8);
	}


	/*
	void GraphRandomTC(int n, int m, int q, int lo, int hi, int edgeDifferentValues, 
		int minPerValue, int minstep, int minChildCount, int maxChildCount, int maxQueryValue = -1, bool isSubtask4 = false)
	*/
	void TestCases() {

		// M = 3000
		CASE(RandomGraph(250, 3000, 0, 1, 10); Q = 0; K = 8);
		CASE(RandomGraph(500, 3000, 0, 1, 10); Q = 0; K = 8);
		CASE(RandomGraph(1000, 3000, 0, 1, 10); Q = 0; K = 8);

		// M = 30000
		CASE(RandomGraph(2500, 30000, 0, 1, 100); Q = 0;K = 8;);
		CASE(RandomGraph(5000, 30000, 0, 1, 100); Q = 0;K = 8;);
		CASE(RandomGraph(10000, 30000, 0, 1, 100); Q = 0;K = 8;);

		// M = 300000
		CASE(RandomGraph(25000, 300000, 0, 1, 1000); Q = 0; K = 8;);
		CASE(RandomGraph(50000, 300000, 0, 1, 1000); Q = 0; K = 8;);
		CASE(RandomGraph(100000, 300000, 0, 1, 1000); Q = 0; K = 8;);
	}

private:

	GraphRandomizer gr;
	// small to large ufds

	void RandomGraph(int nodeCount, int edgeCount, int indexBase, int componentCount, int bridgeCount) {

		Graph g = GraphRandomizer().node_count(nodeCount).edge_count(edgeCount).
		index_base(indexBase).component_count(componentCount).bridge_count(bridgeCount).next();

		N = nodeCount;
		M = edgeCount;

		U = g.edges().first;
		V = g.edges().second;
	}


	vector<int> randomArray(int n, int lo, int hi) {
		vector<int> ret;
		for(int temp=0;temp<n;temp++)
			ret.push_back(rnd.nextInt(lo, hi));
		return ret;
	}


};
