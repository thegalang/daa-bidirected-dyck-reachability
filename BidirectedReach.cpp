#include <bits/stdc++.h>
using namespace std;


class UFDS {

private:
	vector<int> parent;
	vector<int> rank;
	int n;

public:
	UFDS(int _n) {
		n = _n;
		parent.resize(n);
		rank.resize(n);

		for(int temp=0;temp<n;temp++) {
			parent[temp] = temp;
			rank[temp] = 0;
		}

	}

	// using path compression heuristic to get amortized (alpha(N)) complexity
	int find(int x) {
		if(x == parent[x]) return x;

		return parent[x] = find(parent[x]);
	}

	// not using union by rank to ensure that b is always merged to a
	void unite(int a, int b) {
		int x = find(a), y = find(b);
		if(x == y) return;

		parent[y] = x;
	}

	bool isSameSet(int a, int b) {
		return find(a) == find(b);
	}

	// get all equivalence classes
	vector<vector<int>> equivalenceClasses() {

		map<int, vector<int>> classes;
		for(int temp=0;temp<n;temp++) {
			classes[find(temp)].push_back(temp);
		}

		vector<vector<int>> ret;
		for(auto it = classes.begin(); it!=classes.end(); it++) {
			ret.push_back(it->second);
		}

		return ret;
	}
};


struct queuedNode {
	int u, label;
};


UFDS BidirectedReach(int n,int m,int k, vector<unordered_map<int, list<int>>> &adj) {

	UFDS disjointSet = UFDS(n);

	queue<queuedNode> q;

	// find vertices that has two outgoing edges
	// this is O(M) because at the worst case we are iterating over all edges
	for(int v = 0; v < n; v++) {
		for(auto it = adj[v].begin(); it != adj[v].end(); it++) {

			int label = it->first;
			list<int> neighbour = it->second;

			if(neighbour.size() >= 2) {
				q.push({v, label});
			}
		}
	}

	while(!q.empty()) {
		queuedNode qq = q.front();

		q.pop();

		int u = qq.u;
		int label = qq.label;

		/*
		Lets denote SS = sum(|S_i|), where S_i is the S created on the i'th iteration
		The paper proved that SS = O(M)
		*/
		if(u == disjointSet.find(u)) {
			unordered_set<int> s_set;

			// this part is proven to be O(M) total by the paper
			for(auto v : adj[u][label]) {
				s_set.insert(disjointSet.find(v));
			}

			// first put in set, then move to list to get linear performance
			vector<int> S;
			for(auto v : s_set) S.push_back(v);

			// the leader
			int x;
			if(S.size() >= 2) {

				// make the new leader not u
				x = S[0];
				if(u == S[0]) x = S[1];

				// this part is total O(SS alpha(N)) = O(M alpha(N))
				for(auto v : S) {
					disjointSet.unite(x, v);
				}

				// moving the edges from the merged vertices to the new leader
				// this part is total O(SS k) = O(M k).
				for(int edgeLabel = 1; edgeLabel <=k ; edgeLabel++) {

					// iterate all labels this vertex and move it into the new leader
					for(auto v : S) {

						if(u != v || label != edgeLabel) {
							adj[x][edgeLabel].splice(adj[x][edgeLabel].end(), adj[v][edgeLabel]);
						} else {
							adj[x][edgeLabel].push_back(x);
						}
					} 


					if(adj[x][edgeLabel].size() >=2) q.push({x, edgeLabel});

				}
			} else {
				x = S[0];
			}

			// make so this edge connects to the new leader
			if (s_set.find(u) == s_set.end() || S.size() == 1) {
				adj[u][label].push_back(x);
			}

		}
	}

	return disjointSet;
}

// storing in adjaceny list
// eg: 1: { a1 : [2, 3, 5], a2 : [4, 7, 8]}
// zero-based
vector<unordered_map<int, list<int>>> adj;

int main() {
	int n,m, k, temp;
	cin>>n>>m>>k;

	
	adj.resize(n);

	//  dyck edges (u, v, a_label) and (v, u, a_labelHat). Only adds the hat version
	for(int temp=0; temp<m;temp++) {
		int u, v,label;
		cin>>u>>v>>label;
		adj[v][label].push_back(u);
	}

	UFDS disjointSet = BidirectedReach(n ,m, k, adj);

	cout<<"equivalence classes:\n";
	vector<vector<int>> classes = disjointSet.equivalenceClasses();
	for(auto classe : classes) {
		cout<<"{";
		for(auto v : classe) {
			cout<<" "<<v;
		}
		cout<<" }\n";
	}

	int numQueries;
	cin>>numQueries;
	while(numQueries--) {
		int x, y;
		cin>>x>>y;

		if(disjointSet.isSameSet(x, y)) {
			cout<<x<<" and "<<y<<" is Dyck-reachable!\n";
		} else {
			cout<<x<<" and "<<y<<" is not Dyck-reachable!\n";
		}
	}
	

}